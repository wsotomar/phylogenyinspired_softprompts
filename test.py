import argparse
import json
import os
import torch
import wandb

from datasets import Dataset, load_dataset, interleave_datasets, get_dataset_infos, concatenate_datasets

from evaluate import load

from mt5msp_collators import BasicCollator, MSPCollator
from mt5msp_modeling import MT5MSPForConditionalGeneration
from mt5msp_trainer import LMComputeMetrics, LMTrainer

from os import makedirs
from os.path import basename, isdir, join

from transformers import MT5Tokenizer, Seq2SeqTrainingArguments, DataCollatorForSeq2Seq

from torch.utils.data import DataLoader

from tqdm import tqdm

parser = argparse.ArgumentParser(
    prog = 'mt5msp_test',
    description = 'Test a MT5MSP model',
)

parser.add_argument('-tf', '--test_file', type=str, required=True, help='Path to a file with the test configuration', dest='test_file')
parser.add_argument('-bm', '--base_model', type=str, help='Path to the base model', dest='base_model')
parser.add_argument('-bt', '--base_tokenizer', type=str, help='Path to the base tokenizer', dest='base_tokenizer')
parser.add_argument('-sp', '--save_path', type=str, help='Path to the save path', dest='save_path')

if __name__ == '__main__':

    args = parser.parse_args()

    with open(args.test_file, 'r') as file:
        exp_args = json.load(file)

    if args.base_model is not None:
        exp_args['base_model'] = args.base_model

    if args.base_tokenizer is not None:
        exp_args['base_tokenizer'] = args.base_tokenizer

    if args.save_path is not None:
        exp_args['save_path'] = args.save_path

    if 'save_path' not in exp_args:
        exp_args['save_path'] = 'model'

    exp_args['name'] = basename(exp_args['save_path'])

    if 'save_name' not in exp_args:
        exp_args['save_name'] = 'results'

    if 'base_model' not in exp_args:
        exp_args['base_model'] = 'google/mt5-small'

    if 'sub_soft_prompt_values' not in exp_args:
        exp_args['sub_soft_prompt_values'] = [8,2,3,6,2,3,6]

    if 'sub_soft_prompt_lengths' not in exp_args:
        exp_args['sub_soft_prompt_lengths'] = [50,15,15,15,15,15,15]

    if 'base_tokenizer' not in exp_args:
        if 'base_model' in exp_args:
            exp_args[base_tokenizer] = exp_args['base_model']
        else:
            exp_args['base_model'] = 'google/mt5-small'

    if 'max_length' not in exp_args:
        exp_args['max_length'] = 384

    if 'mask_token' not in exp_args:
        exp_args['mask_token'] = '<extra_id_0>'

    if 'batch_size' not in exp_args:
        exp_args['batch_size'] = 2

    if 'streaming' not in exp_args:
        exp_args['streaming'] = True

    if 'device' not in exp_args:
        exp_args['device'] = 'cuda' if torch.cuda.is_available() else 'cpu'

    print('Load tokenizer...')
    tokenizer = MT5Tokenizer.from_pretrained(
        exp_args['base_tokenizer'],
        model_max_length=exp_args['max_length']
    )

    if tokenizer.mask_token_id is None:
        tokenizer.mask_token_id = tokenizer(
            exp_args['mask_token'],
            add_special_tokens=False
        ).input_ids[0]

    print('Instantiate model...')
    model = MT5MSPForConditionalGeneration.from_pretrained(
        exp_args['base_model'],
        sub_soft_prompt_values=exp_args['sub_soft_prompt_values'],
        sub_soft_prompt_lengths=exp_args['sub_soft_prompt_lengths']
    ).to(exp_args['device'])
    model.config.max_length = exp_args['max_length']

    print('Load test datasets...')
    test_datasets = []
    for dataset_name, dataset_params in exp_args['datasets']['test'].items():
        dataset_info = get_dataset_infos(dataset_name)
        for split_name, dataset_configs in dataset_params['splits'].items():
            for config_name in dataset_configs:
                if config_name in dataset_info.keys():
                    temp_ds = load_dataset(
                        dataset_name,
                        config_name,
                        split=split_name,
                        streaming=exp_args['streaming']
                    )
                    temp_ds = temp_ds.map(
                        BasicCollator(tokenizer, **dataset_params['Collator1']),
                        batched=True,
                        remove_columns=dataset_params['RemoveColumns1'],
                    )
                    temp_ds = temp_ds.map(
                        MSPCollator(tokenizer, **exp_args['TestCollator2']),
                        batched=True,
                        remove_columns=exp_args['RemoveColumns2'],
                    )
                    test_datasets.append(temp_ds)

    tokenized_test_ds = concatenate_datasets(
        test_datasets,
        split='test'
    ).with_format('torch')

    test_dataloader = DataLoader(tokenized_test_ds, collate_fn=DataCollatorForSeq2Seq(tokenizer=tokenizer), batch_size=exp_args['batch_size'])

    model.eval()
    decoded_input_ids = []
    decoded_labels = []
    decoded_outputs = []
    with torch.inference_mode():
        print('Computing generations...')
        for batch in tqdm(test_dataloader):
            output = model.generate(
                batch['input_ids'].to(exp_args['device']),
                attention_mask=batch['attention_mask'].to(exp_args['device']),
                keys=batch['keys'].to(exp_args['device']),
                max_new_tokens=exp_args['max_length']
            )

            input_ids = batch['input_ids']
            decoded_input_ids.extend(tokenizer.batch_decode(input_ids, skip_special_tokens=False))
            decoded_input_ids = [sample.replace(tokenizer.pad_token, '') for sample in decoded_input_ids]
            decoded_input_ids = [sample.replace(tokenizer.eos_token, '') for sample in decoded_input_ids]

            labels = batch['labels']
            labels[labels == -100] = tokenizer.pad_token_id
            decoded_labels.extend(tokenizer.batch_decode(labels, skip_special_tokens=False))
            decoded_labels = [sample.replace(tokenizer.pad_token, '') for sample in decoded_labels]
            decoded_labels = [sample.replace(tokenizer.eos_token, '') for sample in decoded_labels]

            decoded_outputs.extend(tokenizer.batch_decode(output, skip_special_tokens=False))
            decoded_outputs = [sample.replace(tokenizer.pad_token, '') for sample in decoded_outputs]
            decoded_outputs = [sample.replace(tokenizer.eos_token, '') for sample in decoded_outputs]

    all_data = list(zip(decoded_input_ids, decoded_outputs, decoded_labels))

    results = {}
    for input_ids, output, label in all_data:
        if input_ids not in results:
            results[input_ids] = {'prediction':output, 'references':[label]}
        else:
            results[input_ids]['references'].append(label)

    if not isdir(exp_args['save_path']):
        makedirs(exp_args['save_path'])

    generations_file_path = join(exp_args['save_path'], exp_args['save_name']+'-generation.json')
    with open(generations_file_path, 'w') as file:
        json.dump(results, file)

    predictions = []
    references = []
    for values in results.values():
        predictions.append(values['prediction'])
        references.append(values['references'])

    max_len = max([len(reference) for reference in references])
    for reference in references:
        while len(reference) < max_len:
            reference.append('')

    bleu = load('sacrebleu')
    ter = load('ter')
    chrf = load('chrf')

    bleu_score = bleu.compute(predictions=predictions, references=references)['score']
    ter_score = ter.compute(predictions=predictions, references=references)['score']
    chrf_score = chrf.compute(predictions=predictions, references=references)['score']

    scores = {
        'BLEU':bleu_score,
        'TER':ter_score,
        'CHRF':chrf_score
    }

    print(scores)

    scores_file_path = join(exp_args['save_path'], exp_args['save_name']+'-scores.txt')
    with open(scores_file_path, 'w') as file:
        json.dump(scores, file)
