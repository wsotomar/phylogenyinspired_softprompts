# PhylogenyInspired_SoftPrompts

Docummentation for the IJCNLP-AACL 2023 paper *Phylogeny-Inspired Soft Prompts For Data-to-Text Generation in Low-Resource Languages*.

## Requirements

Make sure to have the right version of the libraries specified on the `requirements.txt`.

## Download Monolingual Data

Given the size of the monlingual training dataset we stored it outside of the repository. You can download the *Celtic Cllections* dataset from [this link](https://drive.google.com/file/d/1ClNveGNpOHqRszi28SBU2u8B7g6efVP_/view?usp=sharing). Extract the content on the main directory.

## Training

To train a model you just need to run the `train.py` script. You will have to specify the `.json` experiment file with the `-ef` flag. Optionally, you can pass the `-wb` flag to log the progress on Weights & Biases. For example:

```
python train.py -ef experiment_files/step0.json
```

The experiment files used to train Step 0, Step 1, and Step 2 of our paper are available on the `experiment_files` directory.


## Testing

To test a model you just need to run the `test.py` script. YOu will have to specify the `.json` test file with the `-tf` flag, the base model with the `-bm` flag, the base tokenizer with the `-bt` flag anf the safe path with the `-sp` flag. For example:

```
python test.py -tf tes_files/br.json -bm step2 -bt step2 -sp step2
```

The test files used to test Breton, English, Irish and Welsh for our paper are available on the `test_files` directory.