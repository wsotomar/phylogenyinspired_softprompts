import re

from math import ceil
from random import choice, sample, randint
from transformers import DataCollatorForSeq2Seq

def linearize_tripleset(tripleset, clean=True, subj_token='<S>', pred_token='<P>', obj_token='<O>'):
    linearization = ''
    for subj, pred, obj in tripleset:
        linearization += f'<S> {subj} <P> {pred} <O> {obj} '
    if clean:
        linearization = linearization.replace('"','').replace('_',' ')
    while '  ' in linearization:
        linearization = linearization.replace('  ', ' ')
    linearization = linearization.strip()
    linearization = linearization.replace('<S>', subj_token).replace('<P>', pred_token).replace('<O>', obj_token)
    return linearization

def clean_variables(graph):
    variables = [v[1:-3] for v in re.findall(r'\(\w+ / ', graph)]
    substitutes = [f'POINTER{i}' for i in range(len(variables))]
    new_graph = graph
    for var, sub in zip (variables, substitutes):
        new_graph = new_graph.replace(f' {var} ', f' {sub} ')
        new_graph = new_graph.replace(f'{var} / ', f'{sub} ')
    new_graph = re.sub('~\d+', '', new_graph)
    return new_graph

preprocessings ={
    'linearization':linearize_tripleset,
    'cleanvariables':clean_variables,
}

class BasicCollator():

    def __init__(
        self,
        tokenizer,
        src_text_col='text',
        trg_text_col='text',
        src_lang_col='lang',
        trg_lang_col='lang',
        fixed_src_text=None,
        fixed_trg_text=None,
        fixed_src_lang=None,
        fixed_trg_lang=None,
        src_text_preprocess=None,
        trg_text_preprocess=None
    ):
        self.tokenizer = tokenizer
        self.src_text_col = src_text_col
        self.trg_text_col = trg_text_col
        self.src_lang_col = src_lang_col
        self.trg_lang_col = trg_lang_col
        self.fixed_src_text = fixed_src_text
        self.fixed_trg_text = fixed_trg_text
        self.fixed_src_lang = fixed_src_lang
        self.fixed_trg_lang = fixed_trg_lang
        self.src_text_preprocess = preprocessings.get(src_text_preprocess, None)
        self.trg_text_preprocess = preprocessings.get(trg_text_preprocess, None)

    def __call__(self, batch):
        src_text = [text if self.fixed_src_text is None else self.fixed_src_text for text in batch[self.src_text_col]]
        trg_text = [text if self.fixed_trg_text is None else self.fixed_trg_text for text in batch[self.trg_text_col]]
        src_lang = [text if self.fixed_src_lang is None else self.fixed_src_lang for text in batch[self.src_lang_col]]
        trg_lang = [text if self.fixed_trg_lang is None else self.fixed_trg_lang for text in batch[self.trg_lang_col]]


        if self.src_text_preprocess is not None:
            src_text = [self.src_text_preprocess(text) for text in src_text]

        if self.trg_text_preprocess is not None:
            trg_text = [self.trg_text_preprocess(text) for text in trg_text]


        if isinstance(trg_text[0], list):
            data = zip(src_text, trg_text, src_lang, trg_lang)
            data = [[src_text, sub_trg_text, src_lang, trg_lang] for src_text, trg_text, src_lang, trg_lang in data for sub_trg_text in trg_text]
            data = list(zip(*data))
            src_text = list(data[0])
            trg_text = list(data[1])
            src_lang = list(data[2])
            trg_lang = list(data[3])
        elif isinstance(src_text[0], list):
            data = zip(src_text, trg_text, src_lang, trg_lang)
            data = [[sub_src_text, trg_text, src_lang, trg_lang] for src_text, trg_text, src_lang, trg_lang in data for sub_src_text in src_text]
            data = list(zip(*data))
            src_text = list(data[0])
            trg_text = list(data[1])
            src_lang = list(data[2])
            trg_lang = list(data[3])

        batch_size = len(src_text)

        both_text = src_text + trg_text
        tokenized_text = self.tokenizer(both_text, truncation=True)

        src_text = tokenized_text.input_ids[:batch_size]
        trg_text = tokenized_text.input_ids[batch_size:]

        return {
            'src_text':src_text,
            'trg_text':trg_text,
            'src_lang':src_lang,
            'trg_lang':trg_lang,
        }

class MSPCollator():

    def __init__(
        self,
        tokenizer,
        tasks,
        key_format,
        key_mapping,
        lang_mapping,
        sub_soft_prompt_lengths,
        textual_prompt=False,
    ):
        self.tokenizer = tokenizer
        self.textual_prompt = textual_prompt
        self.tasks = tasks
        self.key_format = key_format
        self.key_mapping = key_mapping
        self.sub_soft_prompt_lengths = sub_soft_prompt_lengths
        self.lang_mapping = lang_mapping
        self.collator = DataCollatorForSeq2Seq(tokenizer)

    def __call__(self, batch):

        batch = list(zip(batch['src_text'], batch['trg_text'], batch['src_lang'], batch['trg_lang']))

        input_ids = []
        labels = []
        keys = []
        textual_prompts = []

        counter = 0
        for src_text, trg_text, src_lang, trg_lang in batch:
            counter += 1
            batch_sample = {
                'src_text':src_text,
                'trg_text':trg_text,
                'src_lang':src_lang,
                'trg_lang':trg_lang,
            }
            task = choice(self.tasks)

            if self.textual_prompt:
                src_lang_name = self.lang_mapping[src_lang]
                trg_lang_name = self.lang_mapping[trg_lang]
                textual_prompts.append(self.tokenizer(f'{task} from {src_lang_name} to {trg_lang_name}: ').input_ids)

            if task == 'translation':
                input_ids.append(src_text)
                labels.append(trg_text)

            if task == 'prefixlm':
                half = ceil(len(src_text)/2)
                new_src_text = src_text[:half]+[self.tokenizer.mask_token_id]
                input_ids.append(new_src_text)
                labels.append(trg_text)

            if task == 'suffixlm':
                half = ceil(len(src_text)/2)
                new_src_text = [self.tokenizer.mask_token_id]+src_text[half:]
                input_ids.append(new_src_text)
                labels.append(trg_text)

            if task == 'generation':
                new_src_text = src_text[:1]+[self.tokenizer.mask_token_id]
                input_ids.append(new_src_text)
                labels.append(trg_text)

            if task == 'deshuffling':
                new_src_text = sample(src_text, len(src_text))
                input_ids.append(new_src_text)
                labels.append(trg_text)

            if task == 'maskedlm':
                new_src_text = src_text.copy()
                selected_tokens = sample(range(len(new_src_text)), ceil(len(new_src_text)*0.15))
                masked_tokens = sample(selected_tokens, ceil(len(selected_tokens)*0.80))
                not_masked_tokens = [token for token in selected_tokens if token not in masked_tokens]
                half_not_masked = ceil(len(not_masked_tokens)/2)
                randomized_tokens = not_masked_tokens[:half_not_masked]
                unchanged_tokens = not_masked_tokens[half_not_masked:]

                for position in masked_tokens:
                    new_src_text[position] = self.tokenizer.mask_token_id

                for position in randomized_tokens:
                    new_src_text[position] = randint(0, len(self.tokenizer)-1)

                input_ids.append(new_src_text)
                labels.append(trg_text)

            key = []
            for sub_pormpt, (mapping_table, bathc_sample_column) in self.key_format.items():
                if mapping_table == 'task':
                    key.append(self.key_mapping[mapping_table][task])
                else:
                    key.append(self.key_mapping[mapping_table][batch_sample[bathc_sample_column]])
            keys.append(key)

        if self.textual_prompt:
            input_ids = [textual_prompt+input_id for textual_prompt, input_id in zip(textual_prompts, input_ids)]

        input_ids = [[self.tokenizer.pad_token_id]*sum(self.sub_soft_prompt_lengths)+input_id for input_id in input_ids]

        return {
                'input_ids':input_ids,
                'labels':labels,
                'keys':keys
            }
