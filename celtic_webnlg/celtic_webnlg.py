# coding=utf-8

# Lint as: python3
'''Celtic WebNLG dataset containing sentences in Celtic languages and English.'''

import datasets
import json
from os.path import basename, normpath

logger = datasets.logging.get_logger(__name__)

_DESCRIPTION = '''The CelticWebNLG dataset contains pairs of RDF triplesets and 
natural language lexicalisations in 3 Celtic Languages languages (Breton, Irish, 
and Welsh) as well as English. It is intended for finetuning Celtic RDF-to-Text 
models.'''

_FILE_PATHS = {
    'br':{
        'train':'data/br/br_train.json',
        'validation':'data/br/br_validation.json',
    },
    'cy':{
        'train':'data/cy/cy_train.json',
        'validation':'data/cy/cy_validation.json',
    },
    'en':{
        'train':'data/en/en_train.json',
        'validation':'data/en/en_validation.json',
    },
    'ga':{
        'train':'data/ga/ga_train.json',
        'validation':'data/ga/ga_validation.json',
    },
}

class CelticWebNLGConfig(datasets.BuilderConfig):
    '''BuilderConfig for CelticWebNLG.'''

    def __init__(self, language, **kwargs):
        # Version history:
        # 0.0.1: Initial version.
        super().__init__(
            name=language,
            version='0.0.1',
            **kwargs,
        )
        self.language = language

class CelticWebNLG(datasets.GeneratorBasedBuilder):
    '''Celtic WebNLG dataset.'''

    VERSION = datasets.Version('0.0.1')

    BUILDER_CONFIG_CLASS = CelticWebNLGConfig
    BUILDER_CONFIGS = [
        CelticWebNLGConfig(
            language='br'
        ),
        CelticWebNLGConfig(
            language='cy'
        ),
        CelticWebNLGConfig(
            language='en'
        ),
        CelticWebNLGConfig(
            language='ga'
        ),
    ]

    def _info(self):
        return datasets.DatasetInfo(
            description=_DESCRIPTION,
            features=datasets.Features(
                {
                    'category': datasets.Value('string'),
                    'size': datasets.Value('int32'),
                    'lang': datasets.Value('string'),
                    'tripleset': datasets.Sequence(feature=datasets.Sequence(feature=datasets.Value(dtype='string'))),
                    'lexicalizations': datasets.Sequence(feature=datasets.Value(dtype='string')),
                }
            )
        )
    
    def _split_generators(self, dl_manager: datasets.DownloadManager):
        '''This function generates the splits'''

        data_dir = dl_manager.download(_FILE_PATHS[self.config.name])

        return [
            datasets.SplitGenerator(
                name=datasets.Split.TRAIN,
                gen_kwargs={'filepath': data_dir['train']},
            ),
            datasets.SplitGenerator(
                name=datasets.Split.VALIDATION,
                gen_kwargs={'filepath': data_dir['validation']},
            ),
        ]
    
    def _generate_examples(self, filepath):
        '''This function returns the examples'''
        logger.info('generating examples from = %s', filepath)
        lang, split = basename(normpath(filepath)).split('_')
        with open(filepath, 'r') as file:
            data = json.load(file)
            for id_, sample in enumerate(data):
                yield id_, {
                    'category':sample['category'],
                    'size':sample['size'],
                    'lang':lang,
                    'tripleset':sample['tripleset'],
                    'lexicalizations':sample['lexicalizations']
                }