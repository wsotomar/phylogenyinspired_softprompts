---
annotations_creators:
- no-annotation
configs:
- br
- cy
- en
- ga
language:
- br
- cy
- en
- ga
language_bcp47:
- br-FR
- cy-GB
- en-XX
- ga-IE
language_creators:
- crowdsourced
license:
- unknown
multilinguality:
- multilingual
paperswithcode_id: null
pretty_name: CelticCollection
size_categories:
- n<1K
- 1K<n<10K
- 10K<n<100K
source_datasets:
- web_nlg
task_categories:
- text-generation
task_ids:
- rdf-to-text
dataset_info:
- config_name: br
  features:
  - name: category
    dtype: string
  - name: size
    dtype: int32
  - name: lang
    dtype: string
  - name: tripleset
    sequence:
      sequence: string
  - name: lexicalizations
    sequence: string
  splits:
  - name: train
    num_bytes: 5828207
    num_examples: 13211
  - name: test
    num_bytes: 715074
    num_examples: 1779
  - name: validation
    num_bytes: 463108
    num_examples: 1399
  download_size: 7525971
  dataset_size: 7006389
- config_name: cy
  features:
  - name: category
    dtype: string
  - name: size
    dtype: int32
  - name: lang
    dtype: string
  - name: tripleset
    sequence:
      sequence: string
  - name: lexicalizations
    sequence: string
  splits:
  - name: train
    num_bytes: 6127507
    num_examples: 13211
  - name: test
    num_bytes: 638537
    num_examples: 1779
  - name: validation
    num_bytes: 530882
    num_examples: 1665
  download_size: 7734457
  dataset_size: 7296926
- config_name: en
  features:
  - name: category
    dtype: string
  - name: size
    dtype: int32
  - name: lang
    dtype: string
  - name: tripleset
    sequence:
      sequence: string
  - name: lexicalizations
    sequence: string
  splits:
  - name: train
    num_bytes: 6844638
    num_examples: 13211
  - name: test
    num_bytes: 1085770
    num_examples: 1779
  - name: validation
    num_bytes: 863816
    num_examples: 1667
  download_size: 9167347
  dataset_size: 8794224
- config_name: ga
  features:
  - name: category
    dtype: string
  - name: size
    dtype: int32
  - name: lang
    dtype: string
  - name: tripleset
    sequence:
      sequence: string
  - name: lexicalizations
    sequence: string
  splits:
  - name: train
    num_bytes: 6276647
    num_examples: 13211
  - name: test
    num_bytes: 656857
    num_examples: 1779
  - name: validation
    num_bytes: 547094
    num_examples: 1665
  download_size: 8573081
  dataset_size: 7480598
---

# Dataset Card for Celtic WebNLG

## Table of Contents
- [Dataset Description](#dataset-description)
  - [Dataset Summary](#dataset-summary)
  - [Supported Tasks and Leaderboards](#supported-tasks-and-leaderboards)
  - [Languages](#languages)
- [Dataset Structure](#dataset-structure)
  - [Data Instances](#data-instances)
  - [Data Fields](#data-fields)
  - [Data Splits](#data-splits)
- [Dataset Creation](#dataset-creation)
  - [Curation Rationale](#curation-rationale)
  - [Source Data](#source-data)
  - [Annotations](#annotations)
  - [Personal and Sensitive Information](#personal-and-sensitive-information)
- [Considerations for Using the Data](#considerations-for-using-the-data)
  - [Social Impact of Dataset](#social-impact-of-dataset)
  - [Discussion of Biases](#discussion-of-biases)
  - [Other Known Limitations](#other-known-limitations)
- [Additional Information](#additional-information)
  - [Dataset Curators](#dataset-curators)
  - [Licensing Information](#licensing-information)
  - [Citation Information](#citation-information)
  - [Contributions](#contributions)

### Dataset Summary

The CelticWebNLG dataset contains pairs of RDF triplesets and natural language lexicalisations in 3 Celtic Languages languages (Breton, Irish, and Welsh) as well as English. It is intended for finetuning Celtic RDF-to-Text models.

### Supported Tasks and Leaderboards

The dataset is intended for training Celtic RDF-to-Text models. The performance of such models can be measured with different metrics, including:
- [SacreBLEU](https://huggingface.co/spaces/evaluate-metric/sacrebleu)
- [TER](https://huggingface.co/spaces/evaluate-metric/ter)
- [ChrF](https://huggingface.co/spaces/evaluate-metric/chrf)

### Languages

Here are the available languages in the dataset:

| English name     | Native name | BCP-47 code |
|------------------|-------------|-------------|
| Breton           | Brezhoneg   | br-FR       |
| English          | English     | en-XX       |
| Irish            | Gaeilge     | ga-IE       |
| Welsh            | Cymraeg     | cy-GB       |

## Dataset Structure

### Data Instances

An example from the Irish subset looks as follows:

```
{
    'category': 'Airport',
    'size': 1,
    'lang': 'ga',
    'tripleset': [['Aarhus', 'leader', 'Jacob_Bundsgaard']],
    'lexicalizations': ['Arweinydd Aarhus yw Jacob Bundsgaard.'],
}
```

The structure remains the same across all configurations.

### Data Fields

- category (str): String with the English category of the sample.
- size (int): Integer indicating the number of trilpes in the sammple's tripleset.
- lang (str): String with the ISO 639‑1 code of the lexicalizations language in which the text is written.
- tripleset (List[List[str]): Nested list of triples. Each triple is a list of string in Subject, Predicate, Object order.
- lexicalizations (List[str]): List of strings representing different possible lexicalizations of the tripleset.

### Data Splits

The data is separated in a configuration for each language. Each configuration in turn has a train, dev, and test split.

Here are the number of sample for all the available configurations and splits:

| Language         | Configuration | train | val  |  test |
|------------------|---------------|-------|------|------:|
| Breton           | br            | 13211 | 1399 |  1779 |
| English          | en            | 13211 | 1667 |  5713 |
| Irish            | ga            | 13211 | 1665 |  1779 |
| Welsh            | cy            | 13211 | 1665 |  1779 |

## Dataset Creation

### Curation Rationale

[More Information Needed]

### Source Data

#### Initial Data Collection and Normalization

**Data collection:**
[More Information Needed]

**Normalization:**
[More Information Needed]


#### Who are the source language producers?

The dataset is a collection of text from different sources.

### Annotations

The dataset does not have any annotations.

#### Annotation process

N/A.

#### Who are the annotators?

N/A.

### Personal and Sensitive Information

[More Information Needed]

## Considerations for Using the Data

### Social Impact of Dataset

The dataset is expected to help the development of Celtic DAta-to-Text Models  and help democratize the access to NLP tools to different populations beyond  the English barrier.

### Discussion of Biases

[More Information Needed]

### Other Known Limitations

[More Information Needed]

## Additional Information

### Dataset Curators

[William Soto](https://www.semanticscholar.org/author/William-Soto/2077166607):  PhD Researcher at [Synalp](https://synalp.gitlabpages.inria.fr/synalp-website/),  [LORIA](https://www.loria.fr/en/).

### Licensing Information

[More Information Needed]

### Citation Information

[More Information Needed]

### Contributions

Thanks to [@sotwi](https://github.com/sotwi) for adding this dataset.