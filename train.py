import argparse
import copy
import json
import os
import wandb

from datasets import Dataset, load_dataset, interleave_datasets, get_dataset_infos, concatenate_datasets

from mt5msp_collators import BasicCollator, MSPCollator
from mt5msp_modeling import MT5MSPForConditionalGeneration
from mt5msp_trainer import LMComputeMetrics, LMTrainer

from os.path import basename, join

from random import sample

from transformers import MT5Tokenizer, Seq2SeqTrainingArguments, DataCollatorForSeq2Seq, EarlyStoppingCallback

parser = argparse.ArgumentParser(
    prog = 'mt5msp_train',
    description = 'Train a MT5MSP model',
)

parser.add_argument('-ef', '--experiment_file', type=str, required=True, help='Path to a file with the experiment configuration', dest='experiment_file')
parser.add_argument('-re', '--resume', action='store_true', help='Resume from a previous checkpoint', dest='resume')
parser.add_argument('-wb', '--weight_biases', action='store_true', help='Use weight and biases', dest='weight_biases')

if __name__ == '__main__':

    args = parser.parse_args()

    with open(args.experiment_file, 'r') as file:
        exp_args = json.load(file)

    if 'save_path' not in exp_args:
        exp_args['save_path'] = 'model'

    exp_args['name'] = basename(exp_args['save_path'])

    if 'base_model' not in exp_args:
        exp_args['base_model'] = 'google/mt5-small'

    if 'sub_soft_prompt_values' not in exp_args:
        exp_args['sub_soft_prompt_values'] = [8,2,3,6,2,3,6]

    if 'sub_soft_prompt_lengths' not in exp_args:
        exp_args['sub_soft_prompt_lengths'] = [50,15,15,15,15,15,15]

    if 'base_tokenizer' not in exp_args:
        if 'base_model' in exp_args:
            exp_args[base_tokenizer] = exp_args['base_model']
        else:
            exp_args['base_model'] = 'google/mt5-small'

    if 'max_length' not in exp_args:
        exp_args['max_length'] = 384

    if 'mask_token' not in exp_args:
        exp_args['mask_token'] = '<extra_id_0>'

    if 'stopping_strategy' not in exp_args:
        exp_args['stopping_strategy'] = 'first_exhausted'

    if 'sampling_temperature' not in exp_args:
        exp_args['sampling_temperature'] = 0.3

    if 'batch_size' not in exp_args:
        exp_args['batch_size'] = 2

    if 'eval_batch_size' not in exp_args:
        exp_args['eval_batch_size'] = exp_args['batch_size']

    if 'learning_rate' not in exp_args:
        exp_args['learning_rate'] = 0.1

    if 'lr_scheduler' not in exp_args:
        exp_args['lr_scheduler'] = 'constant'

    if 'weight_decay' not in exp_args:
        exp_args['weight_decay'] = 0.01

    if 'streaming' not in exp_args:
        exp_args['streaming'] = True

    if 'training_epochs' not in exp_args and exp_args['streaming'] == False:
        exp_args['training_epochs'] = 1

    if 'training_steps' not in exp_args:
        if exp_args['streaming'] == True:
            exp_args['training_steps'] = 180_000
        else:
            exp_args['training_steps'] = None

    if 'validation_steps' not in exp_args:
        exp_args['validation_steps'] = 1_000

    if 'predict_with_generate' not in exp_args:
        exp_args['predict_with_generate'] = False

    if 'metric_for_best_model' not in exp_args:
        exp_args['metric_for_best_model'] = 'loss'

    if 'training_size' not in exp_args:
        exp_args['training_size'] = None

    if 'training_mix' not in exp_args:
        exp_args['training_mix'] = 'interleave'

    if 'validation_size' not in exp_args:
        exp_args['validation_size'] = None

    if 'early_stopping' not in exp_args:
        exp_args['early_stopping'] = False
    if exp_args['early_stopping']:
        callbacks = [EarlyStoppingCallback(early_stopping_patience=5)]
    else:
        callbacks = []

    print('Load tokenizer...')
    tokenizer = MT5Tokenizer.from_pretrained(
        exp_args['base_tokenizer'],
        model_max_length=exp_args['max_length']
    )

    if tokenizer.mask_token_id is None:
        tokenizer.mask_token_id = tokenizer(
            exp_args['mask_token'],
            add_special_tokens=False
        ).input_ids[0]

    print('Instantiate model...')
    model = MT5MSPForConditionalGeneration.from_pretrained(
        exp_args['base_model'],
        sub_soft_prompt_values=exp_args['sub_soft_prompt_values'],
        sub_soft_prompt_lengths=exp_args['sub_soft_prompt_lengths']
    )
    model.config.max_length = exp_args['max_length']

    if 'special_tokens' in exp_args:
        print('Add special tokens...')
        special_tokens_dict = {'additional_special_tokens': exp_args['special_tokens']}
        tokenizer.add_special_tokens(special_tokens_dict)
        model.resize_token_embeddings(len(tokenizer))

    print('Save tokenizer...')
    tokenizer.save_pretrained(exp_args['save_path'])

    if 'transfer_sub_prompts' in exp_args:
        for ((trg_sub_prompt, trg_sub_prompt_value), (src_sub_prompt, src_sub_prompt_value)) in exp_args['transfer_sub_prompts']:
            for name, param in model.named_parameters():
                if name == f'multi_soft_prompts.{src_sub_prompt}.{src_sub_prompt_value}':
                    source_param = param
                elif name == f'multi_soft_prompts.{trg_sub_prompt}.{trg_sub_prompt_value}':
                    target_param = param
            target_param.data = copy.deepcopy(source_param.data)

    print('Load training datasets...')
    training_datasets = []
    size = []
    for dataset_name, dataset_params in exp_args['datasets']['train'].items():
        dataset_info = get_dataset_infos(dataset_name)
        for split_name, dataset_configs in dataset_params['splits'].items():
            for config_name in dataset_configs:
                if config_name in dataset_info.keys():
                    temp_ds = load_dataset(
                        dataset_name,
                        config_name,
                        split=split_name,
                        streaming=exp_args['streaming']
                    )

                    if exp_args['training_size'] is not None and not exp_args['streaming']:
                        temp_ds = temp_ds.select(sample(range(len(temp_ds)), exp_args['training_size']))

                    temp_ds = temp_ds.map(
                        BasicCollator(tokenizer, **dataset_params['Collator1']),
                        batched=True,
                        remove_columns=dataset_params['RemoveColumns1'],
                    )
                    temp_ds = temp_ds.map(
                        MSPCollator(tokenizer, **exp_args['Collator2']),
                        batched=True,
                        remove_columns=exp_args['RemoveColumns2'],
                    )
                    training_datasets.append(temp_ds)
                    if exp_args['streaming']:
                        size.append(dataset_info[config_name].splits['train'].num_examples)
                    else:
                        size.append(len(temp_ds))

    print('Set datset smoothing...')
    total_samples = sum(size)
    real_probability = [s/total_samples for s in size]
    adjusted_probability = [p**exp_args['sampling_temperature'] for p in real_probability]
    normalizing_factor = sum(adjusted_probability)
    normalized_adjusted_probability = [p/normalizing_factor for p in adjusted_probability]
    print(normalized_adjusted_probability)

    if exp_args['training_mix'] == 'interleave':
        tokenized_train_ds = interleave_datasets(
            training_datasets,
            probabilities=normalized_adjusted_probability,
            split='train',
            stopping_strategy=exp_args['stopping_strategy']
        ).with_format('torch')
    elif exp_args['training_mix'] == 'concatenate':
        tokenized_train_ds = concatenate_datasets(
            training_datasets,
            split='train'
        ).with_format('torch')

    print('Load validation datasets...')
    validation_datasets = []
    for dataset_name, dataset_params in exp_args['datasets']['validation'].items():
        dataset_info = get_dataset_infos(dataset_name)
        for split_name, dataset_configs in dataset_params['splits'].items():
            for config_name in dataset_configs:
                if config_name in dataset_info.keys():

                    temp_ds = load_dataset(
                        dataset_name,
                        config_name,
                        split=split_name,
                        streaming=False
                    )

                    if exp_args['validation_size'] is not None:
                        temp_ds = temp_ds.select(sample(range(len(temp_ds)), exp_args['validation_size']))

                    temp_ds = temp_ds.map(
                        BasicCollator(tokenizer, **dataset_params['Collator1']),
                        batched=True,
                        remove_columns=dataset_params['RemoveColumns1'],
                    )
                    temp_ds = temp_ds.map(
                        MSPCollator(tokenizer, **exp_args['Collator2']),
                        batched=True,
                        remove_columns=exp_args['RemoveColumns2'],
                    )
                    validation_datasets.append(temp_ds)

    tokenized_validation_ds = concatenate_datasets(
        validation_datasets,
        split='validation'
    ).with_format('torch')

    if 'compute_metrics' not in exp_args:
        if exp_args['predict_with_generate'] == False:
            exp_args['compute_metrics'] = None
        else:
            exp_args['compute_metrics'] = LMComputeMetrics(tokenizer=tokenizer, batch_size=exp_args['batch_size'])
    elif exp_args['compute_metrics'] == True:
        exp_args['compute_metrics'] = LMComputeMetrics(tokenizer=tokenizer, batch_size=exp_args['batch_size'])
    elif exp_args['compute_metrics'] == False:
        exp_args['compute_metrics'] = None

    if not args.weight_biases:
        exp_args['report_to'] = None
        os.environ['WANDB_DISABLED'] = 'true'
    else:
        exp_args['report_to'] = 'wandb'
        wandb.init(project=exp_args['name'], entity='williamsm')

    print('Define training arguments...')
    if exp_args['training_steps'] is not None:
        trainer_args = Seq2SeqTrainingArguments(
            exp_args['save_path'],
            evaluation_strategy='steps',
            eval_steps=exp_args['validation_steps'],
            save_strategy='steps',
            save_steps=exp_args['validation_steps'],
            logging_strategy='steps',
            logging_steps=exp_args['validation_steps'],
            per_device_train_batch_size=exp_args['batch_size'],
            per_device_eval_batch_size=exp_args['eval_batch_size'],
            save_total_limit=3,
            load_best_model_at_end=True,
            max_steps=exp_args['training_steps'],
            report_to=exp_args['report_to'],
            include_inputs_for_metrics=exp_args['include_inputs_for_metrics'],
            predict_with_generate=exp_args['predict_with_generate'],
            lr_scheduler_type = exp_args['lr_scheduler'],
            learning_rate=exp_args['learning_rate'],
            weight_decay=exp_args['weight_decay'],
            metric_for_best_model=exp_args['metric_for_best_model'],
        )
    else:
        trainer_args = Seq2SeqTrainingArguments(
            exp_args['save_path'],
            evaluation_strategy='steps',
            eval_steps=exp_args['validation_steps'],
            save_strategy='steps',
            save_steps=exp_args['validation_steps'],
            logging_strategy='steps',
            logging_steps=exp_args['validation_steps'],
            per_device_train_batch_size=exp_args['batch_size'],
            per_device_eval_batch_size=exp_args['eval_batch_size'],
            save_total_limit=3,
            load_best_model_at_end=True,
            num_train_epochs=exp_args['training_epochs'],
            report_to=exp_args['report_to'],
            include_inputs_for_metrics=exp_args['include_inputs_for_metrics'],
            predict_with_generate=exp_args['predict_with_generate'],
            lr_scheduler_type = exp_args['lr_scheduler'],
            learning_rate=exp_args['learning_rate'],
            weight_decay=exp_args['weight_decay'],
            metric_for_best_model=exp_args['metric_for_best_model'],
        )

    print('Prepare trainer...')
    trainer = LMTrainer(
        model=model,
        args=trainer_args,
        train_dataset=tokenized_train_ds,
        eval_dataset=tokenized_validation_ds,
        data_collator=DataCollatorForSeq2Seq(tokenizer=tokenizer),
        tokenizer=tokenizer,
        compute_metrics=exp_args['compute_metrics'],
        callbacks = callbacks
    )

    if not args.resume:
        print('Run initial evaluation...')
        print(trainer.evaluate())

    print('Run training...')
    print(trainer.train(args.resume))

    print('Save trained model...')
    model.save_pretrained(exp_args['save_path'])

    print('Run final evaluation...')
    print(trainer.evaluate())
